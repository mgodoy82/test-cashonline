package com.test.backendcash.exception;

public class NotFoundDataException extends Exception {

    private final String customMessage;

    public NotFoundDataException(String customMessage) {
        this.customMessage = customMessage;
     }

    public String getCustomMessage() {
        return customMessage;
    }
}
