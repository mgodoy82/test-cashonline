package com.test.backendcash.repositories;

import com.test.backendcash.entity.Loan;
import com.test.backendcash.entity.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Integer> {

    @Query("SELECT loan FROM Loan loan WHERE loan.user = :user")
    List<Loan> findByUser(@Param("user")User user, Pageable pageable);
}