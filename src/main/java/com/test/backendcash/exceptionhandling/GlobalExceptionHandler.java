package com.test.backendcash.exceptionhandling;

import com.test.backendcash.exception.NotFoundDataException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.NoSuchElementException;
import java.util.logging.Logger;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger log = Logger.getLogger("GlobalExceptionHandler");

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public String exceptionHandler(Exception ex){
        log.severe(ex.getMessage());
        return HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchElementException.class)
    public String noDataFound(NoSuchElementException ex){
        log.info(ex.getMessage());
        return "no data found";
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundDataException.class)
    public String noDataFound(NotFoundDataException ex){
        log.info(ex.getCustomMessage());
        log.info(ex.getMessage());
        log.info(ex.getLocalizedMessage());
        log.info(ex.toString());
        return "sin datos encontrados";
    }
}
