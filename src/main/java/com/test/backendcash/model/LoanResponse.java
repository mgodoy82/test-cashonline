package com.test.backendcash.model;

import com.test.backendcash.entity.Loan;

import java.util.function.Function;

public class LoanResponse {
    private Integer id;
    private Integer total;
    private Integer user_id;

    public LoanResponse(Integer id, Integer total, Integer user_id) {
        this.id = id;
        this.total = total;
        this.user_id = user_id;
    }

    public static LoanResponse transformLoanToLoanResponse(Loan loan){
        return new LoanResponse(loan.getId(),
                loan.getTotal(),
                loan.getUser().getId());
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }
}
