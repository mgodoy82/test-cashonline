package com.test.backendcash.model;

import com.test.backendcash.entity.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserResponse implements Serializable {

    private Integer id;
    private String email;
    private String first_name;
    private String last_name;
    private List<LoanResponse> loanResponses;


    public UserResponse(Integer id, String email, String first_name, String last_name, List<LoanResponse> loanResponses) {
        this.id = id;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.loanResponses = loanResponses;
    }

    public static UserResponse transformUserToUserResponse(User user){
        List<LoanResponse> loans = user.getLoans().stream()
                .map(loan -> LoanResponse.transformLoanToLoanResponse(loan))
                .collect(Collectors.toList());

        return new UserResponse(user.getId(),
                user.getEmail(),
                user.getFirst_name(),
                user.getLast_name(),
                loans);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public List<LoanResponse> getLoanResponses() {
        return loanResponses;
    }

    public void setLoanResponses(List<LoanResponse > loanResponses) {
        this.loanResponses = loanResponses;
    }
}
