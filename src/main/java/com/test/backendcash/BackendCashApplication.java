package com.test.backendcash;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories("com.test.backendcash.repositories")
public class BackendCashApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendCashApplication.class, args);
    }
}
