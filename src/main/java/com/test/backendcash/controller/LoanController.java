package com.test.backendcash.controller;

import com.test.backendcash.model.LoanResponse;
import com.test.backendcash.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class LoanController {

    @Autowired
    LoanService service;


    @GetMapping(value = "/loans")
    public List<LoanResponse> findLoans(@RequestParam("user_id") Optional<Integer> userId,
                                        @RequestParam("limit") Integer limit,
                                        @RequestParam("offset") Integer offset){

        if(userId.isPresent())
            return service.findLoansByUserId(userId.get(), offset, limit);
        else
            return service.findLoans(offset, limit);
    }

}
