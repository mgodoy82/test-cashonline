package com.test.backendcash.controller;

import com.test.backendcash.exception.NotFoundDataException;
import com.test.backendcash.model.UserRequest;
import com.test.backendcash.model.UserResponse;
import com.test.backendcash.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/users/{id}")
    public UserResponse getUser(@PathVariable Integer id) throws NotFoundDataException {
        return this.userService.getUser(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/users")
    public void createUser(@RequestBody UserRequest user){
        this.userService.createUser(user);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping(value="/users/{id}")
    public void deleteUser(@PathVariable Integer id){
       this.userService.deleteUser(id);
    }
}
