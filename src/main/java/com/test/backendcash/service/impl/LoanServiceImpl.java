package com.test.backendcash.service.impl;

import com.test.backendcash.entity.Loan;
import com.test.backendcash.entity.User;
import com.test.backendcash.exception.NotFoundDataException;
import com.test.backendcash.model.LoanResponse;
import com.test.backendcash.repositories.LoanRepository;
import com.test.backendcash.repositories.UserRepository;
import com.test.backendcash.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LoanServiceImpl implements LoanService {

    @Autowired
    LoanRepository loanRepository;

    @Autowired
    UserRepository userRepository;


    @Override
    public List<LoanResponse> findLoansByUserId(Integer userId, Integer offset, Integer limit) throws NotFoundDataException {

        Optional<User> user = userRepository.findById(userId);

        if (!user.isPresent())
            throw new NotFoundDataException("LoanServiceImpl.findLoansByUserId() : Not Found Data in ");

        List<Loan> loans = loanRepository.findByUser(user.get(), PageRequest.of(offset,limit));

        return loans.stream()
                .map(loan -> LoanResponse.transformLoanToLoanResponse(loan))
                .collect(Collectors.toList());
    }

    @Override
    public List<LoanResponse> findLoans(Integer offset, Integer limit) {
        List<Loan> loans = loanRepository.findAll(PageRequest.of(offset,limit)).getContent();

        return loans.stream()
                .map(loan -> LoanResponse.transformLoanToLoanResponse(loan))
                .collect(Collectors.toList());
    }
}
