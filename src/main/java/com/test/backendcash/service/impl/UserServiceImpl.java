package com.test.backendcash.service.impl;

import com.test.backendcash.entity.User;
import com.test.backendcash.exception.NotFoundDataException;
import com.test.backendcash.model.UserRequest;
import com.test.backendcash.model.UserResponse;
import com.test.backendcash.repositories.UserRepository;
import com.test.backendcash.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.logging.Logger;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    private static final Logger log = Logger.getLogger("com.test.backendcash.service.impl.UserServiceImpl");

    @Override
    public UserResponse getUser(Integer id) throws NotFoundDataException {

        Optional<User> resp = repository.findById(id);

        if (resp.isPresent())
            return UserResponse.transformUserToUserResponse(resp.get());
        else
            throw new NotFoundDataException("UserRepository.getUser : No Data Found");
    }

    @Override
    public void createUser(UserRequest userRequest) {
        User user = new User(userRequest.getEmail(),
                             userRequest.getFirst_name(),
                             userRequest.getLast_name());
        this.repository.save(user);
    }

    @Override
    public void deleteUser(Integer id) {
        User user = this.repository.findById(id).get();
        this.repository.delete(user);
    }
}
