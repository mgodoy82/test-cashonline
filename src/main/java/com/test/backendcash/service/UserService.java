package com.test.backendcash.service;

import com.test.backendcash.exception.NotFoundDataException;
import com.test.backendcash.model.UserRequest;
import com.test.backendcash.model.UserResponse;

public interface UserService {
    UserResponse getUser(Integer id) throws NotFoundDataException;

    void createUser(UserRequest user);

    void deleteUser(Integer id);
}
