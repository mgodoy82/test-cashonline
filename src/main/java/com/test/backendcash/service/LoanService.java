package com.test.backendcash.service;

import com.test.backendcash.entity.Loan;
import com.test.backendcash.exception.NotFoundDataException;
import com.test.backendcash.model.LoanResponse;

import java.util.List;

public interface LoanService {
    List<LoanResponse> findLoans(Integer offset, Integer limit);

    List<LoanResponse> findLoansByUserId(Integer userId, Integer offset, Integer limit) throws NotFoundDataException;
}
